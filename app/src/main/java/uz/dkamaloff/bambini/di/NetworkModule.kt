package uz.dkamaloff.bambini.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.create
import uz.dkamaloff.bambini.BuildConfig
import uz.dkamaloff.bambini.data.remote.BambiniApiService
import uz.dkamaloff.bambini.data.remote.calladapter.BambiniCallAdapterFactory
import uz.dkamaloff.bambini.data.remote.interceptors.LocaleInterceptor
import uz.dkamaloff.bambini.data.remote.interceptors.TokenInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttp(): OkHttpClient {
        return with(OkHttpClient.Builder()) {
            readTimeout(15, TimeUnit.SECONDS)
            connectTimeout(15, TimeUnit.SECONDS)
            addInterceptor(TokenInterceptor())
            addInterceptor(LocaleInterceptor())
            if (BuildConfig.DEBUG) {
                val loggingInterceptor = HttpLoggingInterceptor { println(it) }
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                addInterceptor(loggingInterceptor)
            }
            build()
        }
    }

    @Provides
    @Singleton
    @OptIn(ExperimentalSerializationApi::class)
    fun provideRetrofit(okHttpClient: OkHttpClient, json: Json): Retrofit {
        val contentType = "application/json".toMediaType()

        return Retrofit.Builder()
            .baseUrl(BambiniApiService.BASE_URL)
            .addCallAdapterFactory(BambiniCallAdapterFactory(json))
            .client(okHttpClient)
            .addConverterFactory(json.asConverterFactory(contentType))
            .build()
    }

    @Provides
    @Singleton
    fun provideBambiniApiService(retrofit: Retrofit): BambiniApiService {
        return retrofit.create()
    }
}
