package uz.dkamaloff.bambini.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import uz.dkamaloff.bambini.data.storage.BambiniDatabase
import uz.dkamaloff.bambini.data.storage.dao.LandingDao
import uz.dkamaloff.bambini.data.storage.dao.PromotionsDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideBambiniDatabase(@ApplicationContext context: Context): BambiniDatabase {
        return Room.databaseBuilder(context, BambiniDatabase::class.java, "bambini_db").build()
    }

    @Provides
    @Singleton
    fun provideLandingDao(database: BambiniDatabase): LandingDao {
        return database.landingDao
    }

    @Provides
    @Singleton
    fun providePromotionsDao(database: BambiniDatabase): PromotionsDao {
        return database.promotionsDao
    }

}
