package uz.dkamaloff.bambini.data.remote.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class BambiniErrorResponse(@SerialName("message") override val message: String) : Throwable()

