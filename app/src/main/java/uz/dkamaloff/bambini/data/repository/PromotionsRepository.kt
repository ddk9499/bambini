package uz.dkamaloff.bambini.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import uz.dkamaloff.bambini.data.remote.BambiniApiService
import uz.dkamaloff.bambini.data.remote.model.BambiniBaseResponse
import uz.dkamaloff.bambini.data.repository.mappers.asEntities
import uz.dkamaloff.bambini.data.storage.dao.PromotionsDao
import uz.dkamaloff.bambini.data.storage.entities.PromotionEntity
import uz.dkamaloff.bambini.domain.exceptions.PromotionsNotFoundException
import javax.inject.Inject

class PromotionsRepository @Inject constructor(
    private val bambiniApi: BambiniApiService,
    private val promotionsDao: PromotionsDao,
) {

    fun flowPromotions(): Flow<List<PromotionEntity>> = flow {
        // emitting cached data
        promotionsDao.getAll().also { if (it.isNotEmpty()) emit(it) }

        fetchAndSync()
        val localData = promotionsDao.getAll()
        if (localData.isEmpty()) throw PromotionsNotFoundException()
        else emit(localData)
    }

    private suspend fun fetchAndSync() {
        try {
            val promotionsResponse = bambiniApi.getPromotionsLine()

            // Why not handling error case? Because of in requirements nothing mentioned about it.
            if (promotionsResponse is BambiniBaseResponse.Success) {
                val entities = promotionsResponse.value.asEntities
                promotionsDao.sync(entities)
            }
        } catch (e: Exception) {
            Timber.tag("PromotionsRepository").e(e)
        }
    }
}
