package uz.dkamaloff.bambini.data.repository.mappers

import uz.dkamaloff.bambini.data.remote.model.LandingResponse
import uz.dkamaloff.bambini.data.remote.model.LandingResponseContentType
import uz.dkamaloff.bambini.data.storage.entities.*

val LandingResponse.asEntities: List<LandingData>
    get() = page.content.map {
        when (it) {
            is LandingResponseContentType.Banner -> it.asEntity
            is LandingResponseContentType.FeaturedCategories -> it.asEntities
            is LandingResponseContentType.Quadro -> it.asEntity
        }
    }

private val LandingResponseContentType.Banner.asEntity: BannerEntity
    get() = BannerEntity(
        size = data.size,
        linkUrl = data.linkUrl,
        imageUrl = data.image.src,
    )

private val LandingResponseContentType.FeaturedCategories.asEntities: FeaturedCategories
    get() = FeaturedCategories(data.categories.map(LandingResponseContentType.Category::asFeaturedCategoryEntity))

private val LandingResponseContentType.Category.asFeaturedCategoryEntity: FeaturedCategoryEntity
    get() = FeaturedCategoryEntity(
        title = title,
        linkUrl = linkUrl,
        imageUrl = image.src
    )

private val LandingResponseContentType.Quadro.asEntity: QuadroWithCategory
    get() = QuadroWithCategory(
        quadro = QuadroEntity(
            title = data.title,
            linkUrl = data.linkUrl,
            imageUrl = data.image.src
        ),
        categories = data.categories.map { quadroCategory ->
            QuadroCategoryEntity(
                title = quadroCategory.title,
                quadroId = 0L,
                linkUrl = quadroCategory.title,
                imageUrl = quadroCategory.image.src,
                backgroundColor = quadroCategory.backgroundColor,
            )
        }
    )
