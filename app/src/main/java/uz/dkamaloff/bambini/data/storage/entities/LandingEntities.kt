package uz.dkamaloff.bambini.data.storage.entities

import androidx.room.*

sealed interface LandingData

@Entity(tableName = "banners")
data class BannerEntity(
    @PrimaryKey(autoGenerate = true) val id: Long = 0L,
    val size: String,
    val linkUrl: String,
    val imageUrl: String,
) : LandingData

@Entity(tableName = "featured_categories")
data class FeaturedCategoryEntity(
    @PrimaryKey(autoGenerate = true) val id: Long = 0L,
    val title: String,
    val linkUrl: String,
    val imageUrl: String,
)

@Entity(tableName = "quadros")
data class QuadroEntity(
    @PrimaryKey(autoGenerate = true) val id: Long = 0L,
    val title: String,
    val linkUrl: String,
    val imageUrl: String,
)

@Entity(tableName = "quadro_categories")
data class QuadroCategoryEntity(
    @PrimaryKey(autoGenerate = true) val id: Long = 0L,
    val quadroId: Long,
    val title: String,
    val linkUrl: String,
    val imageUrl: String,
    val backgroundColor: String
)

data class FeaturedCategories(val categories: List<FeaturedCategoryEntity>) : LandingData

data class QuadroWithCategory(
    @Embedded val quadro: QuadroEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "quadroId"
    )
    val categories: List<QuadroCategoryEntity>,
) : LandingData
