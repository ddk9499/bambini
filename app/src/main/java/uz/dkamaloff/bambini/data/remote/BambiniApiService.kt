package uz.dkamaloff.bambini.data.remote

import retrofit2.http.GET
import uz.dkamaloff.bambini.data.remote.model.BambiniBaseResponse
import uz.dkamaloff.bambini.data.remote.model.LandingResponse
import uz.dkamaloff.bambini.data.remote.model.PromotionsResponse

interface BambiniApiService {

    @GET("v1/user:proline")
    suspend fun getPromotionsLine(): BambiniBaseResponse<PromotionsResponse>

    @GET("v1/page:type=landing")
    suspend fun getLandingPage(): BambiniBaseResponse<LandingResponse>

    companion object {
        const val BASE_URL = "https://babydriver-android.bambinimirror.com/m/"
    }
}
