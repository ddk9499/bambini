package uz.dkamaloff.bambini.data.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import uz.dkamaloff.bambini.data.storage.entities.PromotionEntity

@Dao
interface PromotionsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(promotions: List<PromotionEntity>)

    @Query("SELECT * FROM promotions")
    suspend fun getAll(): List<PromotionEntity>

    @Query("DELETE FROM promotions")
    suspend fun deleteAll()

    @Transaction
    suspend fun sync(promotions: List<PromotionEntity>) {
        deleteAll()
        insert(promotions)
    }
}
