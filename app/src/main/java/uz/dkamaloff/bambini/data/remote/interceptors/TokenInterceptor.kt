package uz.dkamaloff.bambini.data.remote.interceptors

import okhttp3.Interceptor
import okhttp3.Response

private const val API_KEY = "oMXmKN4YSgD8RgeFfMOF54FdyENIxp"

class TokenInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val newRequest = originalRequest.newBuilder()
            .addHeader("bf-api-key", API_KEY)
            .build()

        return chain.proceed(newRequest)
    }
}
