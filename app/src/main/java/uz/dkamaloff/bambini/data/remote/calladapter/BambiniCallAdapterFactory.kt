package uz.dkamaloff.bambini.data.remote.calladapter

import kotlinx.serialization.json.Json
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import uz.dkamaloff.bambini.data.remote.model.BambiniBaseResponse
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class BambiniCallAdapterFactory(private val json: Json) : CallAdapter.Factory() {

    override fun get(
        returnType: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit,
    ): CallAdapter<*, *>? {
        val rawReturnType: Class<*> = getRawType(returnType)
        if (rawReturnType == Call::class.java) {
            if (returnType is ParameterizedType) {
                val callInnerType: Type = getParameterUpperBound(0, returnType)
                if (getRawType(callInnerType) == BambiniBaseResponse::class.java) {
                    // resultType is Call<NewsApiResponse<*>> | callInnerType is NewsApiResponse<*>
                    if (callInnerType is ParameterizedType) {
                        val resultInnerType = getParameterUpperBound(0, callInnerType)
                        return NewsApiResponseCallAdapter<Any?>(resultInnerType, json)
                    }
                    return NewsApiResponseCallAdapter<Nothing>(Nothing::class.java, json)
                }
            }
        }

        return null
    }
}

private class NewsApiResponseCallAdapter<R>(
    private val type: Type,
    private val json: Json,
) : CallAdapter<R, Call<BambiniBaseResponse<R>>> {

    override fun responseType(): Type = type

    override fun adapt(call: Call<R>): Call<BambiniBaseResponse<R>> = BambiniApiCall(call, json)

}
