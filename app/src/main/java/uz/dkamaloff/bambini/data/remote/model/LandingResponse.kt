package uz.dkamaloff.bambini.data.remote.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

@Serializable
data class LandingResponse(
    @SerialName("page") val page: LandingResponsePage,
)

@Serializable
data class LandingResponsePage(@SerialName("content") val content: List<LandingResponseContentType>)

object LandingContentSerializer :
    JsonContentPolymorphicSerializer<LandingResponseContentType>(LandingResponseContentType::class) {

    override fun selectDeserializer(element: JsonElement) =
        when (val name = element.jsonObject["name"]?.jsonPrimitive?.content) {
            "banner" -> LandingResponseContentType.Banner.serializer()
            "featured-categories" -> LandingResponseContentType.FeaturedCategories.serializer()
            "quadro" -> LandingResponseContentType.Quadro.serializer()
            else -> throw Exception("Unknown content type: key '$name' not found or does not matches any module type")
        }
}

@Serializable(with = LandingContentSerializer::class)
sealed interface LandingResponseContentType {

    val name: String

    @Serializable
    data class Banner(
        override val name: String,
        val data: BannerData,
    ) : LandingResponseContentType {

        @Serializable
        data class BannerData(
            @SerialName("size") val size: String,
            @SerialName("linkUrl") val linkUrl: String,
            @SerialName("image") val image: Image,
            @SerialName("caption") val caption: Caption,
        )

        @Serializable
        data class Caption(
            @SerialName("heading") val heading: Heading,
            @SerialName("description") val description: String?,
            @SerialName("cta") val cta: Cta,
            @SerialName("position") val position: Position,
        ) {

            @Serializable
            data class Heading(
                @SerialName("text") val text: String,
                @SerialName("isHidden") val hidden: Boolean,
            )

            @Serializable
            data class Cta(
                @SerialName("text") val text: String?,
                @SerialName("backgroundColor") val backgroundColor: String?,
                @SerialName("textColor") val textColor: String?,
            )

            @Serializable
            data class Position(
                @SerialName("x") val x: String,
                @SerialName("y") val y: String,
            )
        }
    }

    @Serializable
    data class FeaturedCategories(
        override val name: String,
        val data: FeaturedCategoriesData,
    ) : LandingResponseContentType {
        @Serializable
        data class FeaturedCategoriesData(@SerialName("categories") val categories: List<Category>)
    }

    @Serializable
    data class Quadro(
        override val name: String,
        val data: QuadroData,
    ) : LandingResponseContentType {

        @Serializable
        data class QuadroData(
            @SerialName("title") val title: String,
            @SerialName("linkUrl") val linkUrl: String,
            @SerialName("image") val image: Image,
            @SerialName("categories") val categories: List<Category>,
        )
    }

    @Serializable
    data class Category(
        @SerialName("title") val title: String,
        @SerialName("linkUrl") val linkUrl: String,
        @SerialName("image") val image: Image,
        @SerialName("backgroundColor") val backgroundColor: String = "",
    )

    @Serializable
    data class Image(@SerialName("src") val src: String)
}


