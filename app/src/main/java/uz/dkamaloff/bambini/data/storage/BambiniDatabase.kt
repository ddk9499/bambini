package uz.dkamaloff.bambini.data.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import uz.dkamaloff.bambini.data.storage.dao.LandingDao
import uz.dkamaloff.bambini.data.storage.dao.PromotionsDao
import uz.dkamaloff.bambini.data.storage.entities.*

@Database(
    entities = [
        PromotionEntity::class,
        BannerEntity::class,
        FeaturedCategoryEntity::class,
        QuadroEntity::class,
        QuadroCategoryEntity::class,
    ],
    version = 1,
)
abstract class BambiniDatabase : RoomDatabase() {
    abstract val landingDao: LandingDao
    abstract val promotionsDao: PromotionsDao
}
