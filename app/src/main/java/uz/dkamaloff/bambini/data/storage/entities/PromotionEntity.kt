package uz.dkamaloff.bambini.data.storage.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "promotions")
data class PromotionEntity(
    @PrimaryKey(autoGenerate = true) val id: Long = 0L,
    val duration: Int,
    val content: String,
    val backgroundColor: String,
    val textColor: String,
)
