package uz.dkamaloff.bambini.data.remote.calladapter

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.dkamaloff.bambini.data.remote.model.BambiniBaseResponse
import uz.dkamaloff.bambini.data.remote.model.BambiniErrorResponse

class BambiniApiCall<T>(
    proxy: Call<T>,
    private val json: Json,
) : CallDelegate<T, BambiniBaseResponse<T>>(proxy) {

    override fun enqueueImpl(callback: Callback<BambiniBaseResponse<T>>) {
        proxy.enqueue(ResultCallback(this, callback, json))
    }

    override fun cloneImpl(): BambiniApiCall<T> {
        return BambiniApiCall(proxy.clone(), json)
    }

    private class ResultCallback<T>(
        private val proxy: BambiniApiCall<T>,
        private val callback: Callback<BambiniBaseResponse<T>>,
        private val json: Json,
    ) : Callback<T> {

        override fun onResponse(call: Call<T>, response: Response<T>) {
            val result: BambiniBaseResponse<T> = if (response.isSuccessful) {
                BambiniBaseResponse.Success(response.body() as T)
            } else {
                val errorResponse = json.decodeFromString<BambiniErrorResponse>(response.errorBody()?.string().orEmpty())
                BambiniBaseResponse.ServiceError(errorResponse)
            }
            callback.onResponse(proxy, Response.success(result))
        }

        override fun onFailure(call: Call<T>, error: Throwable) {
            callback.onFailure(proxy, error)
        }
    }

    override fun timeout(): Timeout {
        return proxy.timeout()
    }
}
