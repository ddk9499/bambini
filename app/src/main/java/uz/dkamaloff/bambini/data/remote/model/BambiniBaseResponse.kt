package uz.dkamaloff.bambini.data.remote.model

sealed interface BambiniBaseResponse<out T> {
    data class Success<T>(val value: T) : BambiniBaseResponse<T>
    data class ServiceError(val error: BambiniErrorResponse) : BambiniBaseResponse<Nothing>
}
