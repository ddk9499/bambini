package uz.dkamaloff.bambini.data.repository.mappers

import uz.dkamaloff.bambini.data.remote.model.PromoLineItemsResponse
import uz.dkamaloff.bambini.data.remote.model.PromotionsResponse
import uz.dkamaloff.bambini.data.storage.entities.PromotionEntity

val PromotionsResponse.asEntities: List<PromotionEntity>
    get() = user.proline.center.items.map(PromoLineItemsResponse::asEntity)

private val PromoLineItemsResponse.asEntity: PromotionEntity
    get() = PromotionEntity(
        duration = duration,
        content = content,
        // if background color is not sent, by default we select LightGray color
        backgroundColor = highlight?.backgroundColor ?: "#CCCCCC",
        // if textColor color is not sent, by default we select Black color
        textColor = highlight?.textColor ?: "#000000",
    )
