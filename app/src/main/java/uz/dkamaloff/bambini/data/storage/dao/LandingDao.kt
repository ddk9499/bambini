package uz.dkamaloff.bambini.data.storage.dao

import androidx.room.*
import uz.dkamaloff.bambini.data.storage.entities.*

@Dao
interface LandingDao {

    @Transaction
    suspend fun getAll(): List<LandingData> {
        return getAllBanners() + FeaturedCategories(getAllFeaturedCategories()) + getAllQuadroWithCategories()
    }

    @Transaction
    suspend fun sync(landingData: List<LandingData>) {
        deleteAll()
        landingData.forEach { data ->
            when (data) {
                is QuadroWithCategory -> {
                    val quadroId = insertQuadro(data.quadro)
                    val categoriesWithQuadroId =
                        data.categories.map { it.copy(quadroId = quadroId) }
                    insertQuadroCategories(categoriesWithQuadroId)
                }
                is BannerEntity -> insertBanner(data)
                is FeaturedCategories -> data.categories.forEach { insertFeaturedCategory(it) }
            }
        }
    }

    @Transaction
    suspend fun deleteAll() {
        deleteBanners()
        deleteFeaturedCategories()
        deleteQuadros()
        deleteQuadroCategories()
    }

    @Query("DELETE FROM banners")
    suspend fun deleteBanners()

    @Query("DELETE FROM featured_categories")
    suspend fun deleteFeaturedCategories()

    @Query("DELETE FROM quadros")
    suspend fun deleteQuadros()

    @Query("DELETE FROM quadro_categories")
    suspend fun deleteQuadroCategories()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBanner(banner: BannerEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFeaturedCategory(featuredCategory: FeaturedCategoryEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertQuadro(quadro: QuadroEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertQuadroCategories(quadroCategories: List<QuadroCategoryEntity>)

    @Query("SELECT * FROM banners")
    suspend fun getAllBanners(): List<BannerEntity>

    @Query("SELECT * FROM featured_categories")
    suspend fun getAllFeaturedCategories(): List<FeaturedCategoryEntity>

    @Transaction
    @Query("SELECT * FROM quadros")
    suspend fun getAllQuadroWithCategories(): List<QuadroWithCategory>

}
