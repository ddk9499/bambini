package uz.dkamaloff.bambini.data.remote.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PromotionsResponse(@SerialName("user") var user: UserResponse)

@Serializable
data class UserResponse(@SerialName("proline") var proline: PromoLineResponse)

@Serializable
data class PromoLineResponse(@SerialName("center") var center: CenterResponse)

@Serializable
data class CenterResponse(@SerialName("items") var items: List<PromoLineItemsResponse>)

@Serializable
data class PromoLineItemsResponse(
    @SerialName("duration") var duration: Int,
    @SerialName("content") var content: String,
    @SerialName("highlight") var highlight: PromoLineItemsHighlight?,
)

@Serializable
data class PromoLineItemsHighlight(
    @SerialName("backgroundColor") var backgroundColor: String,
    @SerialName("textColor") var textColor: String,
)
