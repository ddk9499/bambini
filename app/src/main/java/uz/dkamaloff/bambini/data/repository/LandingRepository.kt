package uz.dkamaloff.bambini.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import uz.dkamaloff.bambini.data.remote.BambiniApiService
import uz.dkamaloff.bambini.data.remote.model.BambiniBaseResponse
import uz.dkamaloff.bambini.data.repository.mappers.asEntities
import uz.dkamaloff.bambini.data.storage.dao.LandingDao
import uz.dkamaloff.bambini.data.storage.entities.LandingData
import uz.dkamaloff.bambini.domain.exceptions.LandingItemsNotFoundException
import javax.inject.Inject

class LandingRepository @Inject constructor(
    private val bambiniApi: BambiniApiService,
    private val landingDao: LandingDao,
) {

    fun flowLandingData(): Flow<List<LandingData>> = flow {
        // emitting cached data
        landingDao.getAll().also { if (it.isNotEmpty()) emit(it) }

        fetchAndSync()
        val localData = landingDao.getAll()
        if (localData.isEmpty()) throw LandingItemsNotFoundException()
        else emit(localData)
    }

    private suspend fun fetchAndSync() {
        try {
            val landingResponse = bambiniApi.getLandingPage()

            // Why not handling error case? Because of in requirements nothing mentioned about it.
            if (landingResponse is BambiniBaseResponse.Success) {
                val entities = landingResponse.value.asEntities
                landingDao.sync(entities)
            }
        } catch (e: Exception) {
            Timber.tag("LandingRepository").e(e)
        }
    }
}
