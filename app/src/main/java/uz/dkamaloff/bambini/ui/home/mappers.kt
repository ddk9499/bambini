package uz.dkamaloff.bambini.ui.home

import androidx.compose.ui.graphics.Color
import uz.dkamaloff.bambini.common.Outcome
import uz.dkamaloff.bambini.domain.model.*
import uz.dkamaloff.bambini.ui.utls.parseHtmlToAnnotatedString

fun Outcome<HomeScreenItems>.toHomeScreenState(): HomeScreenState = when (this) {
    is Outcome.Failure -> HomeScreenState.Error
    is Outcome.Progress -> HomeScreenState.Loading
    is Outcome.Success -> data.toHomeScreenContentState()
}

private fun HomeScreenItems.toHomeScreenContentState(): HomeScreenState.Content =
    HomeScreenState.Content(
        promotionUiModels = PromotionsUiModel(promotions.map(PromotionDomain::toUiModel)),
        landingUiModels = LandingUiItemsModel(landingItems.map(LandingItemDomain::toUiModel)),
    )

private fun PromotionDomain.toUiModel(): PromotionUiModel = PromotionUiModel(
    text = parseHtmlToAnnotatedString(content),
    duration = duration,
    backgroundColor = Color(android.graphics.Color.parseColor(backgroundColor)),
    textColor = Color(android.graphics.Color.parseColor(textColor)),
)

private fun LandingItemDomain.toUiModel(): LandingUiModel = when (this) {
    is BannerDomain -> BannerUiModel(size = size, link = link, imageUrl = imageUrl)
    is FeaturedCategoriesDomain -> FeaturedCategoriesUiModel(
        categories
            .map(FeaturedCategoryDomain::toUiModel)
            .windowed(size = 2, step = 2)
    )
    is QuadroDomain -> QuadroUiModel(
        quadroData = quadroData.toUiModel(),
        windowedCategories = categories
            .map(QuadroCategoryDomain::toUiModel)
            .windowed(size = 2, step = 2)
    )
}

private fun FeaturedCategoryDomain.toUiModel(): FeaturedCategoryUiModel =
    FeaturedCategoryUiModel(title = title, link = link, imageUrl = imageUrl)

private fun QuadroDataDomain.toUiModel(): QuadroDataUiModel =
    QuadroDataUiModel(title = title, link = link, imageUrl = imageUrl)

private fun QuadroCategoryDomain.toUiModel(): QuadroCategoryUiModel =
    QuadroCategoryUiModel(
        title = title,
        link = link,
        imageUrl = imageUrl,
        backgroundColor = Color(android.graphics.Color.parseColor(backgroundColor)),
    )
