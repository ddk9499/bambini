package uz.dkamaloff.bambini.ui.home

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.delay

@OptIn(ExperimentalPagerApi::class)
@Composable
fun PromotionsPager(
    modifier: Modifier = Modifier,
    promotions: PromotionsUiModel,
) {
    val pagerState = rememberPagerState()
    val currentPromotion by remember(pagerState.currentPage) {
        mutableStateOf(promotions.data[pagerState.currentPage])
    }

    LaunchedEffect(promotions, pagerState.currentPage) {
        delay(currentPromotion.duration.toLong())
        pagerState.scrollToPage((pagerState.currentPage + 1) % promotions.data.size)
    }

    HorizontalPager(
        modifier = modifier,
        state = pagerState,
        count = promotions.data.size,
        userScrollEnabled = false,
    ) { _ ->
        PromotionPagerItem(promotionUiModel = currentPromotion)
    }
}

@Composable
private fun PromotionPagerItem(
    modifier: Modifier = Modifier,
    promotionUiModel: PromotionUiModel,
) {
    Box(modifier = modifier
        .background(promotionUiModel.backgroundColor)
        .padding(vertical = 8.dp)
        .wrapContentHeight()
        .fillMaxWidth()
    ) {
        Text(
            modifier = Modifier.align(Alignment.Center),
            text = promotionUiModel.text,
            color = promotionUiModel.textColor,
        )
    }
}
