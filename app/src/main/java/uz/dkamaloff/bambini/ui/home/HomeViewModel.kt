package uz.dkamaloff.bambini.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.stateIn
import uz.dkamaloff.bambini.domain.FlowHomeScreenDataUseCase
import javax.inject.Inject

@OptIn(ExperimentalCoroutinesApi::class)
@HiltViewModel
class HomeViewModel @Inject constructor(
    homeScreenDataUseCase: FlowHomeScreenDataUseCase,
) : ViewModel() {

    val screenState: StateFlow<HomeScreenState> = homeScreenDataUseCase
        .invoke()
        .mapLatest { outcomeItems -> outcomeItems.toHomeScreenState() }
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = HomeScreenState.Loading,
        )

}
