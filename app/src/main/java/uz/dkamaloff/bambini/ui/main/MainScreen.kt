package uz.dkamaloff.bambini.ui.main

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.util.fastForEach
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import uz.dkamaloff.bambini.R
import uz.dkamaloff.bambini.ui.HomeScreenRoute

@Composable
fun MainScreen(
    modifier: Modifier = Modifier,
) {
    val navController = rememberNavController()
    Scaffold(
        modifier = modifier,
        topBar = { BambiniAppBar() },
        bottomBar = { BambiniBottomNavigationBar(navController) }
    ) { innerPadding ->
        SetupNavigation(modifier = Modifier.padding(innerPadding), navController = navController)
    }
}

@Composable
private fun SetupNavigation(modifier: Modifier, navController: NavHostController) {
    NavHost(
        modifier = modifier,
        navController = navController,
        startDestination = BottomNavigationItems.Home.route
    ) {
        bottomNavigationItems.fastForEach { bottomNavScreen ->
            when (bottomNavScreen) {
                BottomNavigationItems.Home -> composable(bottomNavScreen.route) {
                    HomeScreenRoute(modifier = Modifier.fillMaxSize())
                }
                else -> composable(bottomNavScreen.route) {
                    PageIsNotImplemented(
                        modifier = Modifier.fillMaxSize(),
                        pageName = stringResource(id = bottomNavScreen.label),
                    )
                }
            }
        }
    }
}

@Composable
private fun PageIsNotImplemented(
    modifier: Modifier = Modifier,
    pageName: String,
) {
    Box(modifier = modifier) {
        Text(
            modifier = Modifier.align(Alignment.Center),
            text = "$pageName is not implemented yet",
        )
    }
}

@Composable
private fun BambiniAppBar(modifier: Modifier = Modifier) {
    Box(modifier = modifier.fillMaxWidth().height(56.dp).background(Color.Black)) {
        Text(
            modifier = Modifier.align(Alignment.Center),
            text = "BAMBINIFASHION.COM",
            color = Color.White,
        )
    }
}

@Composable
private fun BambiniBottomNavigationBar(navController: NavController) {
    BottomNavigation {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentDestination = navBackStackEntry?.destination
        bottomNavigationItems.fastForEach { screen ->
            BottomNavigationItem(
                icon = { Icon(imageVector = screen.icon, contentDescription = null) },
                label = { Text(text = stringResource(id = screen.label)) },
                selected = currentDestination?.hierarchy?.any { it.route == screen.route } == true,
                onClick = {
                    navController.navigate(screen.route) {
                        // Pop up to the start destination of the graph to
                        // avoid building up a large stack of destinations
                        // on the back stack as users select items
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        // Avoid multiple copies of the same destination when
                        // reselecting the same item
                        launchSingleTop = true
                        // Restore state when reselecting a previously selected item
                        restoreState = true
                    }
                }
            )
        }
    }
}

@Preview(device = Devices.PIXEL_XL, showBackground = true, showSystemUi = true)
@Preview(device = Devices.PIXEL_3, showBackground = true, showSystemUi = true)
@Composable
fun BambiniAppBarPreview() {
    MainScreen(Modifier.fillMaxSize())
}

private val bottomNavigationItems = listOf(
    BottomNavigationItems.Menu,
    BottomNavigationItems.Home,
    BottomNavigationItems.Bag,
    BottomNavigationItems.Search,
    BottomNavigationItems.Designers,
)

private sealed class BottomNavigationItems(
    val route: String,
    @StringRes val label: Int,
    val icon: ImageVector,
) {
    object Menu : BottomNavigationItems(
        route = "menu_tab",
        label = R.string.bottom_nav_item_menu,
        icon = Icons.Default.Menu,
    )

    object Home : BottomNavigationItems(
        route = "home_tab",
        label = R.string.bottom_nav_item_home,
        icon = Icons.Default.Home,
    )

    object Bag : BottomNavigationItems(
        route = "bag_tab",
        label = R.string.bottom_nav_item_bag,
        icon = Icons.Default.ShoppingCart,
    )

    object Search : BottomNavigationItems(
        route = "search_tab",
        label = R.string.bottom_nav_item_search,
        icon = Icons.Default.Search,
    )

    object Designers : BottomNavigationItems(
        route = "designers_tab",
        label = R.string.bottom_nav_item_designers,
        icon = Icons.Default.Star,
    )
}
