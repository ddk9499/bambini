package uz.dkamaloff.bambini.ui.home

import androidx.compose.runtime.Immutable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString

sealed interface HomeScreenState {
    object Loading : HomeScreenState

    data class Content(
        val promotionUiModels: PromotionsUiModel,
        val landingUiModels: LandingUiItemsModel,
    ) : HomeScreenState

    object Error : HomeScreenState
}

@Immutable
data class PromotionsUiModel(val data: List<PromotionUiModel>)

@Immutable
data class LandingUiItemsModel(val data: List<LandingUiModel>)

@Immutable
data class PromotionUiModel(
    val text: AnnotatedString,
    val duration: Int,
    val backgroundColor: Color,
    val textColor: Color,
)

sealed interface LandingUiModel

@Immutable
data class BannerUiModel(
    val size: String,
    val link: String,
    val imageUrl: String,
) : LandingUiModel

@Immutable
data class FeaturedCategoriesUiModel(val windowedCategories: List<List<FeaturedCategoryUiModel>>) : LandingUiModel

@Immutable
data class FeaturedCategoryUiModel(
    val title: String,
    val link: String,
    val imageUrl: String,
)

@Immutable
data class QuadroUiModel(
    val quadroData: QuadroDataUiModel,
    val windowedCategories: List<List<QuadroCategoryUiModel>>,
) : LandingUiModel

@Immutable
data class QuadroDataUiModel(
    val title: String,
    val link: String,
    val imageUrl: String,
)

@Immutable
data class QuadroCategoryUiModel(
    val title: String,
    val link: String,
    val imageUrl: String,
    val backgroundColor: Color,
)
