package uz.dkamaloff.bambini.ui.home

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.util.fastForEach
import coil.compose.AsyncImage

private val paddingBetweenElements = 12.dp

@Composable
fun LandingUi(
    modifier: Modifier = Modifier,
    landingUiItemsModel: LandingUiItemsModel,
) {
    Column(
        modifier = modifier
            .padding(paddingBetweenElements)
            .background(Color.White),
        verticalArrangement = Arrangement.spacedBy(paddingBetweenElements)
    ) {
        landingUiItemsModel.data.fastForEach { item ->
            when (item) {
                is BannerUiModel -> BannerUi(imageUrl = item.imageUrl)
                is FeaturedCategoriesUiModel -> FeaturedCategoriesUi(featuredCategories = item)
                is QuadroUiModel -> QuadroUi(quadroUiModel = item)
            }
        }
    }
}

@Composable
private fun BannerUi(
    modifier: Modifier = Modifier,
    imageUrl: String,
) {
    AsyncImage(
        modifier = modifier
            .fillMaxWidth()
            .aspectRatio(1F),
        model = imageUrl,
        contentScale = ContentScale.Crop,
        contentDescription = null,
    )
}

@Composable
private fun FeaturedCategoriesUi(
    modifier: Modifier = Modifier,
    featuredCategories: FeaturedCategoriesUiModel,
) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(paddingBetweenElements)
    ) {
        featuredCategories.windowedCategories.fastForEach { categories ->
            Row(horizontalArrangement = Arrangement.spacedBy(paddingBetweenElements)) {
                categories.fastForEach { category ->
                    CategoryItemUi(
                        modifier = Modifier.weight(1F).aspectRatio(0.75F),
                        imageUrl = category.imageUrl,
                        title = category.title,
                    )
                }
            }
        }
    }
}

@Composable
private fun CategoryItemUi(
    modifier: Modifier = Modifier,
    imageUrl: String,
    title: String,
) {
    Box(modifier = modifier) {
        AsyncImage(
            modifier = Modifier.fillMaxSize(),
            model = imageUrl,
            contentDescription = null,
            contentScale = ContentScale.Crop,
        )
        Text(
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = paddingBetweenElements)
                .background(Color.White, RoundedCornerShape(2.dp))
                .padding(
                    vertical = paddingBetweenElements / 2,
                    horizontal = paddingBetweenElements * 2
                ),
            text = title,
            color = Color.Black,
        )
    }
}

@Composable
private fun QuadroUi(
    modifier: Modifier = Modifier,
    quadroUiModel: QuadroUiModel,
) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(paddingBetweenElements)
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            text = quadroUiModel.quadroData.title,
            textAlign = TextAlign.Center,
            fontFamily = FontFamily.Serif,
            fontSize = 26.sp,
            fontWeight = FontWeight.Bold,
        )
        BannerUi(imageUrl = quadroUiModel.quadroData.imageUrl)
        QuadroCategories(quadroUiModel = quadroUiModel)
    }
}

@Composable
private fun QuadroCategories(
    modifier: Modifier = Modifier,
    quadroUiModel: QuadroUiModel,
) {
    quadroUiModel.windowedCategories.fastForEach { categories ->
        Row(
            modifier = modifier,
            horizontalArrangement = Arrangement.spacedBy(paddingBetweenElements)
        ) {
            categories.fastForEach { category ->
                QuadroCategoryItemUi(
                    modifier = Modifier.weight(1F).aspectRatio(0.75F),
                    imageUrl = category.imageUrl,
                    title = category.title,
                    backgroundColor = category.backgroundColor,
                )
            }
        }
    }
}

@Composable
private fun QuadroCategoryItemUi(
    modifier: Modifier,
    imageUrl: String,
    title: String,
    backgroundColor: Color,
) {
    Column(
        modifier = modifier.background(backgroundColor),
    ) {
        AsyncImage(
            modifier = Modifier.fillMaxSize().weight(3 / 4F),
            model = imageUrl,
            contentDescription = null,
            contentScale = ContentScale.Crop,
        )
        Box(
            modifier = Modifier
            .align(Alignment.CenterHorizontally)
            .weight(1 / 4F)
        ) {
            Text(
                modifier = Modifier.align(Alignment.Center),
                text = title,
                color = Color.Black,
                textAlign = TextAlign.Center,
            )
        }
    }
}
