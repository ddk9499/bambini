package uz.dkamaloff.bambini.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import uz.dkamaloff.bambini.ui.home.HomeScreenState
import uz.dkamaloff.bambini.ui.home.HomeViewModel
import uz.dkamaloff.bambini.ui.home.LandingUi
import uz.dkamaloff.bambini.ui.home.PromotionsPager

@OptIn(ExperimentalLifecycleComposeApi::class)
@Composable
fun HomeScreenRoute(
    modifier: Modifier = Modifier,
    viewModel: HomeViewModel = hiltViewModel(),
) {
    val screenState by viewModel.screenState.collectAsStateWithLifecycle()

    when (val state = screenState) {
        is HomeScreenState.Content -> HomeScreenContent(modifier = modifier, content = state)
        HomeScreenState.Error -> HomeScreenError(modifier = modifier)
        HomeScreenState.Loading -> HomeScreenLoading(modifier = modifier)
    }
}

@Composable
private fun HomeScreenContent(
    modifier: Modifier = Modifier,
    content: HomeScreenState.Content,
) {
    val scrollState = rememberScrollState()
    Column(
        modifier = modifier.verticalScroll(scrollState)
    ) {
        PromotionsPager(promotions = content.promotionUiModels)
        LandingUi(
            modifier = Modifier.fillMaxSize(),
            landingUiItemsModel = content.landingUiModels
        )
    }
}

@Composable
private fun HomeScreenError(
    modifier: Modifier = Modifier,
) {
    Box(modifier = modifier) {
        Text(modifier = Modifier.align(Alignment.Center), text = "Page is empty")
    }
}

@Composable
private fun HomeScreenLoading(
    modifier: Modifier = Modifier,
) {
    Box(modifier = modifier) {
        CircularProgressIndicator(modifier = Modifier.align(Alignment.Center).size(100.dp))
    }
}
