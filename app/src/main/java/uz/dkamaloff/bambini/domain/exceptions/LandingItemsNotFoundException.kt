package uz.dkamaloff.bambini.domain.exceptions

class LandingItemsNotFoundException : Throwable("Landing items not found")
