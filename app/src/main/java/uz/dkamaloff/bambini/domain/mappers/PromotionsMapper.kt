package uz.dkamaloff.bambini.domain.mappers

import uz.dkamaloff.bambini.data.storage.entities.PromotionEntity
import uz.dkamaloff.bambini.domain.model.PromotionDomain

val PromotionEntity.asDomainObject: PromotionDomain
    get() = PromotionDomain(
        duration = duration,
        content = content,
        backgroundColor = backgroundColor,
        textColor = textColor,
    )
