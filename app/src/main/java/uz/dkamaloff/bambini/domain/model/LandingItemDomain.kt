package uz.dkamaloff.bambini.domain.model

sealed interface LandingItemDomain {
    val order: Int get() = Int.MAX_VALUE
}

data class BannerDomain(
    override val order: Int = 1,
    val size: String,
    val link: String,
    val imageUrl: String,
) : LandingItemDomain

data class FeaturedCategoriesDomain(
    override val order: Int = 2,
    val categories: List<FeaturedCategoryDomain>,
) : LandingItemDomain

data class FeaturedCategoryDomain(
    val title: String,
    val link: String,
    val imageUrl: String,
)

data class QuadroDomain(
    val quadroData: QuadroDataDomain,
    val categories: List<QuadroCategoryDomain>,
) : LandingItemDomain

data class QuadroDataDomain(
    val title: String,
    val link: String,
    val imageUrl: String,
)

data class QuadroCategoryDomain(
    val title: String,
    val link: String,
    val imageUrl: String,
    val backgroundColor: String,
)
