package uz.dkamaloff.bambini.domain.exceptions

class PromotionsNotFoundException : Throwable("Promotions not found")
