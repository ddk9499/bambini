package uz.dkamaloff.bambini.domain.mappers

import uz.dkamaloff.bambini.data.storage.entities.*
import uz.dkamaloff.bambini.domain.model.*

val LandingData.asDomainObject: LandingItemDomain
    get() = when (this) {
        is BannerEntity -> this.asDomainObject
        is FeaturedCategories -> this.asDomainObject
        is QuadroWithCategory -> this.asDomainObject
    }

private val BannerEntity.asDomainObject: BannerDomain
    get() = BannerDomain(
        size = size,
        link = linkUrl,
        imageUrl = imageUrl
    )

private val FeaturedCategories.asDomainObject: FeaturedCategoriesDomain
    get() = FeaturedCategoriesDomain(categories = categories.map(FeaturedCategoryEntity::asDomainObject))

private val FeaturedCategoryEntity.asDomainObject: FeaturedCategoryDomain
    get() = FeaturedCategoryDomain(
        title = title,
        link = linkUrl,
        imageUrl = imageUrl
    )

private val QuadroWithCategory.asDomainObject: QuadroDomain
    get() = QuadroDomain(
        quadroData = QuadroDataDomain(
            title = quadro.title,
            link = quadro.linkUrl,
            imageUrl = quadro.imageUrl,
        ),
        categories = categories.map(QuadroCategoryEntity::asDomainObject),
    )

private val QuadroCategoryEntity.asDomainObject: QuadroCategoryDomain
    get() = QuadroCategoryDomain(
        title = title,
        link = linkUrl,
        imageUrl = imageUrl,
        backgroundColor = backgroundColor,
    )
