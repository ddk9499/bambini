package uz.dkamaloff.bambini.domain.model

data class HomeScreenItems(
    val promotions: List<PromotionDomain>,
    val landingItems: List<LandingItemDomain>
)
