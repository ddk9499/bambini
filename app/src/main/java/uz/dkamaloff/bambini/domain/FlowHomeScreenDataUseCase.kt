package uz.dkamaloff.bambini.domain

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import timber.log.Timber
import uz.dkamaloff.bambini.common.Outcome
import uz.dkamaloff.bambini.data.repository.LandingRepository
import uz.dkamaloff.bambini.data.repository.PromotionsRepository
import uz.dkamaloff.bambini.data.storage.entities.LandingData
import uz.dkamaloff.bambini.data.storage.entities.PromotionEntity
import uz.dkamaloff.bambini.domain.mappers.asDomainObject
import uz.dkamaloff.bambini.domain.model.HomeScreenItems
import javax.inject.Inject

class FlowHomeScreenDataUseCase @Inject constructor(
    private val promotionsRepository: PromotionsRepository,
    private val landingRepository: LandingRepository,
) {

    operator fun invoke(): Flow<Outcome<HomeScreenItems>> = combine(
        landingRepository.flowLandingData(),
        promotionsRepository.flowPromotions(),
    ) { landingData, promotions ->
        Outcome.success(mapToHomeScreenItems(promotions, landingData))
    }
        .onStart { emit(Outcome.loading()) }
        .catch { e ->
            Timber.tag("FlowHomeScreenDataUseCase").e(e)
            emit(Outcome.failure(e))
        }
        .flowOn(Dispatchers.IO)

    private fun mapToHomeScreenItems(
        promotionsData: List<PromotionEntity>,
        landingData: List<LandingData>,
    ): HomeScreenItems = HomeScreenItems(
        promotions = promotionsData.map(PromotionEntity::asDomainObject),
        landingItems = landingData.map(LandingData::asDomainObject).sortedBy { it.order }
    )
}
