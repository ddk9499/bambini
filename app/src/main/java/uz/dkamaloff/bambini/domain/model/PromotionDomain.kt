package uz.dkamaloff.bambini.domain.model

data class PromotionDomain(
    val duration: Int,
    val content: String,
    val backgroundColor: String,
    val textColor: String,
)
