package uz.dkamaloff.bambini.common

/**
 * Class to wrap the data, with it is status, following the Loading/Content/Error (LCE) pattern
 */
sealed interface Outcome<T> {
    data class Progress<T>(val loading: Boolean, val partialData: T? = null) : Outcome<T>
    data class Success<T>(val data: T) : Outcome<T>
    data class Failure<T>(val e: Throwable, val partialData: T? = null) : Outcome<T>

    companion object {
        fun <T> loading(
            isLoading: Boolean = true,
            partialData: T? = null,
        ): Outcome<T> = Progress(isLoading, partialData)

        fun <T> success(data: T): Outcome<T> = Success(data)

        fun <T> failure(e: Throwable, partialData: T? = null): Outcome<T> = Failure(e, partialData)
    }
}
